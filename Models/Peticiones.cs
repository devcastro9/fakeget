﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using Microsoft.EntityFrameworkCore;

namespace FakeGet.Models
{
    public partial class Peticiones
    {
        [Key]
        [Column("id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Column("tipo")]
        [JsonPropertyName("tipo_peticion")]
        public int Tipo { get; set; }
        [Column("llave")]
        [StringLength(50)]
        [Unicode(false)]
        public string Llave { get; set; } = null!;
        [Column("fecha", TypeName = "datetime")]
        [JsonPropertyName("fecha_peticion")]
        public DateTime Fecha { get; set; }
    }
}
