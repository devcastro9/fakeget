﻿using FakeGet.Data;
using FakeGet.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FakeGet.Controllers
{
    [Route("api")]
    [ApiController]
    public class FakeController : ControllerBase
    {
        private readonly AppDbContext _context;
        private readonly ILogger<FakeController> _logger;
        public FakeController(AppDbContext context, ILogger<FakeController> logger)
        {
            _context = context;
            _logger = logger;
        }

        [HttpGet("pago")]
        public async Task<IActionResult> GetPagoExitoso(string id)
        {
            Peticiones obj = new()
            {
                Tipo = 1,
                Llave = id,
                Fecha = DateTime.Now
            };
            _context.Add(obj);
            await _context.SaveChangesAsync();
            _logger.LogInformation("Peticion 1 guardada");
            return StatusCode(201);
        }
        [HttpGet("pago/{id}")]
        public async Task<ActionResult<Peticiones>> GetPago(int id)
        {
            if (_context.Peticiones == null)
            {
                return NotFound();
            }
            Peticiones? peticion = await _context.Peticiones.FindAsync(id);
            if (peticion == null)
            {
                return NotFound();
            }
            return peticion;
        }

        [HttpGet("url")]
        public async Task<IActionResult> GetUrlRetorno(string id)
        {
            Peticiones obj = new()
            {
                Tipo = 2,
                Llave = id,
                Fecha = DateTime.Now
            };
            _context.Add(obj);
            await _context.SaveChangesAsync();
            _logger.LogInformation("Peticion 2 guardada");
            return Ok();
        }

        [HttpGet("peticiones")]
        public async Task<ActionResult<IEnumerable<Peticiones>>> GetPeticiones()
        {
            return await _context.Peticiones.AsNoTracking().ToListAsync();
        }
    }
}
