# FakeGet
Un Web Service con ruta Fake.
## Comandos utiles
Comando de construccion de la imagen Docker
```
sudo docker build -t aspnet-fakeget -f Dockerfile .
```
Comando para ejecutar la imagen Docker
```
sudo docker run -d -p 5000:80 -p 5001:443 --name otisapp aspnet-fakeget
```
