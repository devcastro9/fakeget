FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY ["FakeGet.csproj", "."]
RUN dotnet restore "./FakeGet.csproj"
COPY . .
WORKDIR "/src/."
RUN dotnet build "FakeGet.csproj" -c Release -o /app/build --nologo

FROM build AS publish
RUN dotnet publish "FakeGet.csproj" -c Release -o /app/publish --nologo

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "FakeGet.dll"]